React学习

react的优点
组件模式：代码复用的团队分工
虚拟Dom:性能优势
移动端支持：跨终端

react的缺点
学习成本高
全新的一套概念，与其他所有框架截然不同
只有采用他的整个技术栈，才能发挥最大威力

JSX语法

react使用jsx语法
let myTitle=<h1>Hello react</h1>

JSX语法解释
1.JSX语法的最外层只能有一个节点
2.jsx语法中可以插入js代码，使用大括号
let myTitle =<p>{'Hello'+'World'}</p>

Babel转码器
Js引擎（包括浏览器和node）都不认识jsx,需要首先使用babel转码才可以使用
React需要加载两个库：React和React-dom,前者是React的核心库，后者是react的Dom适配库
Babel用来在浏览器转换jsx,如果服务器已经好了，浏览器就不需要加载这个库

practice1.html实现练习1中的点击hello world ，world变成当今的日期  
practice2.html实现练习2中的点击hello world,world变化，再次点击world变回来，这里的思路是设置一个变量isclicked每次点击之后都把她的值置反，再用三目运算符进行复制


react相关的框架：react-bootstrap  ant-design react
react 图表框架   http://recharts.org/    HightCharts


React的核心思想
view是State的输出
view=f(state)
上式中f表示函数关系，只要state发生变化，函数也要变化
React的本质是将图形界面函数化

React存在的问题：
由于react本事知识一个DOM的抽象层，使用组件构建虚拟DOM，如果开发大应用，还需要解决
1：大型应用程序如何组织代码？
2：组件之间如何通信
 关于react的通信问题：
 向子组件传递，向父组件传递，向其他组件传递，然而react只提供一种传递方式：传参，对于大型应用很不方便

目前最流行的两个react架构：
Mobx响应式管理，state是可变对象，适合中小型项目
redux:函数式，state是不可变对象，适合大型项目

@observer是一种新的语法，叫做“装饰器”，表示对整个类的行为进行修改

Redux架构
React的核心概念
所有的状态在store上，组件每次重新渲染，都必须由状态变化引起
用户在UI上发出action
reducer函数接收action，然后根据当前的state,计算出新的state

redux层保存所有的状态，react组件拿到状态以后，渲染出html页面

可以进行数据处理、并包含状态的组件，成为“容器组件”，Redux使用connect方法，自动生成UI组件对应的“容器组件”
mapStateToprops函数返回一个对象，表示一种映射关系，将UI组件的参数映射到state
mapDispathToProps函数也是返回一个对象，表示一种映射关系，但定义的是哪些用户的操作应该当做action传给store
reducer函数用来接收action，算出新的state。
Store由redux提供的createStore方法生成，该方法接受reducer作为参数
为了把store传入组件，必须使用redux提供的provider在应用的最外层包裹一层
Redux将组件分成UI组件和容器组件两类
UI组件是纯组件，不含state和生命周期方法，不涉及组件的行为，只涉及组件的外观
容器组件正好相反
不涉及组件的外观，只涉及组件的行为
负责订阅store。讲store的数据处理以后，再通过参数传递给UI组件
用户给出配置以后，由Redux生成
mapStateToprops:定义UI组件参数与State之间的映射
mapDispatchToProps:定义UI组件与Action之间的映射

拆分 UI 组件和容器组件的好处

UI 组件与后台数据无关，可以由设计师负责
容器组件只负责数据和行为，一旦 Store 的数据结构变化，只要调整容器组件即可
表现层和功能层脱钩，有利于代码重用，也有利于看清应用的数据结构和业务逻辑

HTTP动词
操作       SQL方法    HTTP动词
create    insert     post
read      select     get
update    update     put
delete    delete     delete

在http请求中，get可以请求到信息，如果想要post，put，delete,就需要用到以下两句话
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
body-parser模块的作用，是对POST、PUT、DELETE等 HTTP 方法的数据体进行解析。app.use用来将这个模块加载到当前应用。有了这两句，就可以处理POST、PUT、DELETE等请求了。

https://github.com/expressjs/response-time
可以打印出请求服务器的时间，详情可以查看此网站
