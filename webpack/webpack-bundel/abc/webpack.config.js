var path = require('path');
var webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const hotMiddlewareScript = 'webpack-hot-middleware/client?reload=true';
const HtmlWebpackPlugin = require('html-webpack-plugin');

const webpackDevServer = require('webpack-dev-server');
const config = require("./webpack.config.js");

//installed via npm

module.exports = {
  entry: [
  //  'webpack-dev-server/client?http://localhost:3000',
    hotMiddlewareScript, './src/index.js'
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/dist/'
  },
  module: {
      rules: [
          {
              test: /\.html$/,
              use: ['raw-loader'] // loaders: ['raw-loader'] is also perfectly acceptable.
          }, {
              test: /\.js?$/,
              exclude: /(node_modules|bower_components)/,
              use: ['babel-loader']
          }, {
              test: /\.css$/,
              use: ExtractTextPlugin.extract({fallback: "style-loader", use: "css-loader"})
          }, {
              test: /\.less$/,
              use: ExtractTextPlugin.extract({
                  fallback: "style-loader",
                  use: ['css-loader', 'less-loader']
              })
          }, {
              test: /\.js$/,
              enforce: "post",
              use: ['es3ify-loader']
          }
      ]
  },
      plugins: [
          new webpack.BannerPlugin('这个插件用来写几个字'),
          // ExtractTextPlugin，抽取CSS代码
          new ExtractTextPlugin("bundle.css"),
          // CommonsChunkPlugin，抽取JS公共代码
          new webpack.optimize.CommonsChunkPlugin({name: "commons", filename: "commons.js"}),
          // UglifyJsPlugin，压缩JS代码
          new webpack.optimize.UglifyJsPlugin(),
          new HtmlWebpackPlugin({template: './index.html'}),
          new webpack.HotModuleReplacementPlugin()
      ],
      devServer: {
          contentBase: path.join(__dirname, "/"),
          compress: true,
          port: 3000
      }
};
