const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config.js');
const compiler = webpack(webpackConfig);

const app = express();

app.use(express.static(path.join(__dirname,"dist")));

app.use(require("webpack-dev-middleware")(compiler, {
    publicPath: webpackConfig.output.publicPath,
    stats: {
        colors: true
    }
}));

app.use(require("webpack-hot-middleware")(compiler));

app.listen(3000, 'localhost', function(err) {
    if (err) {
        console.log(err);
        return;
    }
    console.log('Server listening on http://localhost:3000')
});
