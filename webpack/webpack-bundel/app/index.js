import _ from 'lodash';
//require("!style-loader!css-loader!./index.css");  loader才能直接引用css文件

require("../index.html");
const user = require("./user.js");

var App=document.getElementById("root");
App.innerHTML=user.name;

require("./index.css");//需要在webpack.config.js中配置
function component () {
  var element = document.createElement('div');

  /* lodash is required for the next line to work */
  element.innerHTML = _.join(['Hello','react1'], ' ');

  return element;
}

document.body.appendChild(component());
